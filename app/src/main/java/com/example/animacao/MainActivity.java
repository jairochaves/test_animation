package com.example.animacao;

import android.animation.Animator;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView textView,textView2,textView3;
    int width;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*textView=findViewById(R.id.anima);
        textView.setVisibility(View.INVISIBLE);
        textView2=findViewById(R.id.anima2);
        textView2.setVisibility(View.INVISIBLE);
        textView3=findViewById(R.id.anima3);
        textView3.setVisibility(View.INVISIBLE);*/
    }
    public void clica(View view){
        ScaleAnimation animation=new ScaleAnimation(1,10 ,0,10,10,10);
        TranslateAnimation translateAnimation = new TranslateAnimation(0,100,0,100);
        animation.setDuration(5000);
        translateAnimation.setDuration(5000);
        //textView.setAnimation(animation);
        //textView.startAnimation(translateAnimation);

       /* animationT(textView3,1000);
        animationT(textView,2000);
        animationT(textView2,3000);*/
        //textView3.setHeight(textView3.getWidth());
        startActivity(new Intent(MainActivity.this,Lottie.class));

    }
    public void animationT(TextView textView,long time){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // get the center for the clipping circle
            int cx = textView.getWidth() / 2;
            int cy = textView.getHeight() / 2;

            // get the final radius for the clipping circle
            float finalRadius = (float) Math.hypot(cx, cy);

            // create the animator for this view (the start radius is zero)
            Animator anim = ViewAnimationUtils.createCircularReveal(textView, cx, cy, 10f, finalRadius);
            anim.setDuration(time);
            anim.setInterpolator(new OvershootInterpolator());

            // make the view visible and start the animation
            textView.setVisibility(View.VISIBLE);
            anim.start();
        } else {
            // set the view to invisible without a circular reveal animation below Lollipop
            textView.setVisibility(View.INVISIBLE);
        }
    }
}
