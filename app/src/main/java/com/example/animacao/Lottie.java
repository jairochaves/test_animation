package com.example.animacao;

import android.animation.Animator;
import android.content.Context;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.TextView;

public class Lottie extends AppCompatActivity {
    TextView textView,textView2,textView3;
    Button buttonAnima;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lottie);
        textView=findViewById(R.id.anima);
        textView.setVisibility(View.INVISIBLE);

        textView2=findViewById(R.id.anima2);
        textView2.setVisibility(View.INVISIBLE);

        textView3=findViewById(R.id.anima3);
        textView3.setVisibility(View.INVISIBLE);

        buttonAnima=findViewById(R.id.btn_anima);
        buttonAnima.setVisibility(View.INVISIBLE);


        //
        textView.post(new Runnable() {
            @Override
            public void run() {
                animationT(textView,2000);
                animationT(textView3,1000);
                animationT(textView2,3000);
                animationT(buttonAnima,1500);
            }
        });
    }

    public void button(View view){
        Animation animation=new TranslateAnimation(0,0,0,-3);
        buttonAnima.startAnimation(animation);
    }
    public void animationT(View textView, long time){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // get the center for the clipping circle
            int cx = textView.getWidth() / 2;
            int cy = textView.getHeight() / 2;

            // get the final radius for the clipping circle
            float finalRadius = (float) Math.hypot(cx, cy);

            // create the animator for this view (the start radius is zero)
            Animator anim = ViewAnimationUtils.createCircularReveal(textView, cx, cy, 1f, finalRadius);
            anim.setDuration(time);
            anim.setInterpolator(new OvershootInterpolator());

            // make the view visible and start the animation
            textView.setVisibility(View.VISIBLE);
            anim.start();
        } else {
            // set the view to invisible without a circular reveal animation below Lollipop
            textView.setVisibility(View.INVISIBLE);
        }
    }
}
